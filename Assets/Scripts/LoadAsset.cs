﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

#if WINDOWS_UWP
using Windows.Storage;
using Windows.Storage.AccessCache;
using Windows.Storage.Pickers;
using Windows.Storage.Provider;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
#endif
#if UNITY_EDITOR
using UnityEditor;
#endif
#if !UNITY_EDITOR && UNITY_WSA
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
#endif

public class LoadAsset : MonoBehaviour {

    public GameObject myObject;

    // Use this for initialization
    void Start()
    {
        string path = null;

#if !UNITY_EDITOR && UNITY_WSA
        path = Application.persistentDataPath;
        //pathHolo = ApplicationData.Current.RoamingFolder.Path;
#else
        path = "http://tulipsol.com/models/museum";
#endif

        Debug.Log("pathPC: " + path);
        Debug.Log(String.Format("Loading mesh file at: ", Path.Combine(path, "cube")));

        string url = Path.Combine(path, "cube");
        WWW www = new WWW(url);
        StartCoroutine(WaitForReq(www));
    }

    IEnumerator WaitForReq(WWW www)
    {
        yield return www;
        Debug.Log("Finish downloading.");
        AssetBundle bundle = www.assetBundle;
        if (www.error == null)
        {
            //GameObject gameobject = (GameObject)bundle.LoadAsset("bird");
            //myObject = bundle.LoadAsset("seagull_headphone.glb") as GameObject;
            myObject = bundle.LoadAsset("cube") as GameObject;

            Debug.Log("Assets loaded.");
            Instantiate(myObject);
            Debug.Log("Instantiated.");
        }
        else
        {
            Debug.Log(www.error);
        }
    }

}
