﻿using UnityEngine;
using UnityEditor;

public class CreateAssetBundles : MonoBehaviour {

    [MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles", BuildAssetBundleOptions.None, BuildTarget.WSAPlayer);
    }

    /*[MenuItem("Assets/Build AssetBundles")]
    static void BuildMapABs()
    {
        // Create the array of bundle build details.
        // This array of buildMap contains numbers of assets bundle
        // we want to build. Each bundle has assetBundleName and assetNames.
        // In this case, we only need to build 1 bundle asset
        // of the bird, so the size is 1
        AssetBundleBuild[] buildMap = new AssetBundleBuild[1];

        string[] enemyAssets = new string[2];
        enemyAssets[0] = "Assets/Textures/Rock-Texture-Surface.jpg";
        enemyAssets[1] = "Assets/Textures/7.png";

        buildMap[0].assetBundleName = "museum";
        buildMap[0].assetNames = enemyAssets;


        //buildMap[0].assetBundleName = "test";0

        //string[] enemyAssets = new string[2];
        //enemyAssets[0] = "Assets/Textures/5.png";
        //enemyAssets[1] = "Assets/Textures/7.png";

        //buildMap[0].assetNames = enemyAssets;
        //buildMap[1].assetBundleName = "bird";

        //string[] heroAssets = new string[1];
        //heroAssets[0] = "char_hero_beanMan";
        //buildMap[1].assetNames = heroAssets;

        BuildPipeline.BuildAssetBundles("Assets/AssetBundles", buildMap, BuildAssetBundleOptions.None, BuildTarget.WSAPlayer);*/
}

